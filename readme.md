
# ************************************************
# INSTACION SCIP & PYSCIPOPT
# ************************************************

# Instalacion de SCIPOptSuite para Windows

1º Instalar SCIPOptSuite-7.0.2-win64-VS15
2º ejecutar 
	>_scip
	>_read <file_name>.zpl
	>_optimize
	>_display solution
	>_write solution <file_name>.sol
3º Fin del prueba

# Instalacion de pyscipopt en Windows

1º Abrir el cmd y ejecutar:
>_set SCIPOPTDIR="C:\Program Files\SCIPOptSuite 7.0.2"

2º Instalar VS_BuildTools
>_ Ir a https://visualstudio.microsoft.com/es/visual-cpp-build-tools/
>_ y descargar lo siguiente
    C++ build tools y seleccionar * 
        * MSVC v142 - VS 2019 C++ x64..
        * Windows 10 SDK
3º Reiniciar la PC

4º Ejecutar
>_pip install --upgrade setuptools

5º Ejecutar
>_pip install pyscipopt

# ************************************************
# COMANDOS DE EJECUCION
# ************************************************

# Ejecucion de python
>_python <file_name>.py

# Ejecucion SCIP
>_scip
>_read <file_name>.zpl
>_optimize
>_display solution
>_write solution <file_name>.sol

# Ejecucion en serie de comandos scip
scip -c "read <file_name>.zpl optimize display solution"

# ************************************************
# LINKS DE REF. PySCIPOpt
# ************************************************

# Ejemplo de minimize y maximize
* ref.: https://imada.sdu.dk/~marco/Misc/PySCIPOpt/finished_2logical_8py_source.html

# Ejemplo de indicadores de variables
* ref.: https://stackoverflow.com/questions/62632101/indicator-variable-in-pyscipopt

# Ejemplo uso de quicksum
* ref.: https://programtalk.com/python-examples/pyscipopt.quicksum/
* ref.: https://programtalk.com/vs2/python/11626/PySCIPOpt/tests/test_quicksum.py/

# ************************************************
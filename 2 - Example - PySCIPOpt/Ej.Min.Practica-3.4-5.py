# ***********************************
# * EJERCICIO de Practica -> 3.4-5. *
# ***********************************

# Variable definitions
# var x >= 0;
# var y >= 0;

# Objective function
# minimize obj: + 15*x + 20*y;

# Constraints
# subto R1: + 1*x + 2*y >= 10;
# subto R2: + 2*x - 3*y <= 6;
# subto R3: + 1*x + 1*y >= 6;

# ***********************************

from pyscipopt import Model

# model name is optional
model = Model("Example")  

# Variables
x = model.addVar("x", vtype="INTEGER")
y = model.addVar("y", vtype="INTEGER")

# Funcion objetivo
model.setObjective(15*x + 20*y, sense="minimize")

# Restricciones
model.addCons(1*x + 2*y >= 10)
model.addCons(2*x - 3*y <= 6)
model.addCons(1*x + 1*y >= 6)

# Optimizacion
model.optimize()
sol = model.getBestSol()

# Mostrar resultados
print("x: {}".format(sol[x]))
print("y: {}".format(sol[y]))

# ***********************************
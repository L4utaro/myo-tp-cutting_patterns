
class GeneradorPDC:

    # ****************************************************************************
    # ************************* Declaracion de variables *************************
    # ****************************************************************************

    def __init__(self, cortes, tamanioBarra):
        self.contadorPatrones = 0;
        self.punteroEstatico = 0;
        self.punteroDinamico = 0;
        self.cortes = cortes;
        self.tamanioBarra = tamanioBarra;
        self.indices = range(len(self.cortes));
        self.patron = [];
        for i in self.indices:
            self.patron.append(0);
        self.terminar = False;
        # variable de salida
        self.patrones=[];
        # variables auxs entre metodos.
        self.desde=0;
        self.materialSobrante=0;

    # ****************************************************************************
    # ******************************* Sub-Metodos ********************************
    # ****************************************************************************

    def resto(self):
        acum = 0;
        for i in self.indices:
            acum = self.patron[i] * self.cortes[i] + acum;
        return self.tamanioBarra-acum;

    def crearPatron(self):
        sobrante = self.materialSobrante;
        indice = self.punteroDinamico + 1;
        while indice != len(self.cortes):
            cant = sobrante // self.cortes[indice];
            sobrante = sobrante % self.cortes[indice];
            self.patron[indice] = cant;
            indice = indice + 1;
            
    def Cuentas(self):
        self.materialSobrante = self.resto();
        self.crearPatron();
    # ****************************************************************************
    def PersistirPatron(self):
        nuevo_patron=[];
        nuevo_patron.append(self.contadorPatrones);
        patronOut = str(self.contadorPatrones);
        for i in self.indices:
            nuevo_patron.append(self.patron[i]);
        self.patrones.append(nuevo_patron);
        self.contadorPatrones = self.contadorPatrones + 1;
    # ****************************************************************************
    def FinalizarCiclo(self):
        if self.punteroEstatico == len(self.indices)-1 & self.punteroEstatico == self.punteroDinamico:
            self.terminar = True;
    # ****************************************************************************
    def limpiarArrayPatronDesde(self):
        index = self.desde;
        while index <= len(self.patron)-1:
            self.patron[index] = 0;
            index = index + 1;
    
    def maximizarPunteroEstaticoDesde(self):
        index = self.desde;
        sobrante = self.resto();
        while index < len(self.patron):
            valorMaxPuntEsta = sobrante // self.cortes[index];
            if valorMaxPuntEsta != 0:
                self.punteroEstatico = index;
                self.patron[self.punteroEstatico] =  valorMaxPuntEsta;
                return;
            index = index + 1;
        self.punteroEstatico = len(self.patron)-1;

    def maximizarPunteroDinamicoDesde(self):
        index = self.desde;
        sobrante = self.resto();
        while index < len(self.patron):
            valorMaxPuntDina = sobrante // self.cortes[index];
            if valorMaxPuntDina != 0:
                self.punteroDinamico = index;
                self.patron[self.punteroDinamico] =  valorMaxPuntDina;
                return;
            index = index + 1;
        self.punteroDinamico = len(self.patron)-1;
    
    def MoverPunteros(self):
        if self.terminar == True:
            return;
        else:
            if self.punteroDinamico == len(self.patron)-1:
                self.patron[self.punteroEstatico] = self.patron[self.punteroEstatico] - 1;
                self.desde = self.punteroEstatico+1;
                self.limpiarArrayPatronDesde();
                self.maximizarPunteroDinamicoDesde();
                if self.patron[self.punteroEstatico] == 0:
                    self.desde=0;
                    self.limpiarArrayPatronDesde();
                    self.desde = self.punteroEstatico+1;
                    self.maximizarPunteroEstaticoDesde();
                    self.desde = self.punteroEstatico+1;
                    self.maximizarPunteroDinamicoDesde();
            else:
                self.patron[self.punteroDinamico] = self.patron[self.punteroDinamico] - 1;
                self.desde=self.punteroDinamico+1;
                self.limpiarArrayPatronDesde();
                if self.patron[self.punteroDinamico] == 0:
                    self.desde=self.punteroDinamico+1;
                    self.maximizarPunteroDinamicoDesde();

    # ****************************************************************************
    # ***************************** Metodo Principal *****************************
    # ****************************************************************************

    def generarPatrones(self):
        self.desde = 0;
        self.maximizarPunteroEstaticoDesde();
        self.desde = self.punteroEstatico;
        self.maximizarPunteroDinamicoDesde();
        while self.terminar == False:
            #Cuentas
            self.Cuentas();
            #PersistirPatron
            self.PersistirPatron();
            #FinalizarCiclo
            self.FinalizarCiclo();
            #MoverPunteros
            self.MoverPunteros();
            #Para cerrar prueba
        return self.patrones;

    # ****************************************************************************
    # ****************************************************************************
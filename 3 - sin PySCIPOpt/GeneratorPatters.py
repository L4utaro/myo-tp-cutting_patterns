# ****************************************************************************
# ************************* Declaracion de variables *************************
# ****************************************************************************
indices = [0,1,2,3];
contadorPatrones = 1;
punteroEstatico = 0;
punteroDinamico = 0;
patron = [0,0,0,0];
cortes = [135,108,93,42];
tamanioBarra = 300;
terminar = False;
# ****************************************************************************
# ******************************* Sub-Metodos ********************************
# ****************************************************************************
def resto():
    acum = 0;
    for i in indices:
        acum = patron[i] * cortes[i] + acum;
    return tamanioBarra-acum;

def crearPatron(sobrante):
    global patron;
    indice = punteroDinamico + 1;
    while indice != len(cortes):
        cant = sobrante // cortes[indice];
        sobrante = sobrante % cortes[indice];
        patron[indice] = cant;
        indice = indice + 1;
        
def Cuentas():
    sobrante = resto();
    crearPatron(sobrante);
# ****************************************************************************
def PersistirPatron(archivo):
    global contadorPatrones;
    patronOut = str(contadorPatrones);
    for i in indices:
        patronOut = patronOut+','+str(patron[i]);
    if contadorPatrones != 1:
        archivo.write('\n');    
    archivo.write(patronOut);
    contadorPatrones = contadorPatrones + 1;
# ****************************************************************************
def FinalizarCiclo():
    if punteroEstatico == len(indices)-1 & punteroEstatico == punteroDinamico:
        global terminar;
        terminar = True;
# ****************************************************************************
def limpiarArrayPatronDesde(index):
    global patron;
    while index <= len(patron)-1:
        patron[index] = 0;
        index = index + 1;

def maximizarPunteroEstaticoDesde(index):
    global punteroEstatico;
    global patron;
    sobrante = resto();
    while index < len(patron):
        valorMaxPuntEsta = sobrante // cortes[index];
        if valorMaxPuntEsta != 0:
            punteroEstatico = index;
            patron[punteroEstatico] =  valorMaxPuntEsta;
            return;
        index = index + 1;
    punteroEstatico = len(patron)-1;

def maximizarPunteroDinamicoDesde(index):
    global punteroDinamico;
    global patron;
    sobrante = resto();
    while index < len(patron):
        valorMaxPuntDina = sobrante // cortes[index];
        if valorMaxPuntDina != 0:
            punteroDinamico = index;
            patron[punteroDinamico] =  valorMaxPuntDina;
            return;
        index = index + 1;
    punteroDinamico = len(patron)-1;

def MoverPunteros():
    global punteroEstatico;
    global punteroDinamico;
    global patron;
    if terminar == True:
        return;
    else:
        if punteroDinamico == len(patron)-1:
            patron[punteroEstatico] = patron[punteroEstatico] - 1;
            limpiarArrayPatronDesde(punteroEstatico+1);
            maximizarPunteroDinamicoDesde(punteroEstatico+1);
            if patron[punteroEstatico] == 0:
                limpiarArrayPatronDesde(0);
                maximizarPunteroEstaticoDesde(punteroEstatico+1);
                maximizarPunteroDinamicoDesde(punteroEstatico+1);
        else:
            patron[punteroDinamico] = patron[punteroDinamico] - 1;
            limpiarArrayPatronDesde(punteroDinamico+1);
            if patron[punteroDinamico] == 0:
                maximizarPunteroDinamicoDesde(punteroDinamico+1);
# ****************************************************************************
# ***************************** Metodo Principal *****************************
# ****************************************************************************
maximizarPunteroEstaticoDesde(0);
maximizarPunteroDinamicoDesde(punteroEstatico);
archivo = open('input.txt','w')
while terminar == False:
    #Cuentas
    Cuentas();
    #PersistirPatron
    PersistirPatron(archivo);
    #FinalizarCiclo
    FinalizarCiclo();
    #MoverPunteros
    MoverPunteros();
    #Para cerrar prueba
archivo.close()
# ****************************************************************************
# ****************************************************************************
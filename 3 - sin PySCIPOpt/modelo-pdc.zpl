# *********************************************************************************************************************************
# * EJERCICIO en Clases - Patrones de corte *
# *********************************************************************************************************************************

# *********************************************************************************************************************************
# CONSIGNA
# Cuál es la mínima cantidad de rollos de $3$ metros de ancho que necesitamos fabricar para satisfacer la siguiente demanda?
# 97 rollos de 135 cm de ancho. (w)
# 610 rollos de 108 cm de ancho. (x)
# 395 rollos de 93 cm de ancho. (y)
# 211 rollos de 42 cm de ancho. (z)
# *********************************************************************************************************************************

# Lectura de inputs (patrones de corte)
set IWXYZ := { read "input.txt" as "<1n,2n,3n,4n,5n>" comment "#" };
set N := { 1 .. card(IWXYZ) };

# Declaracion de variables
var p[N] integer >= 0;

# Funcion objetivo
minimize cantidadDeRollos : sum <i> in N : p[i];

# Restricciones
subto restRolloW: ( sum <i,w,x,y,z> in IWXYZ :
                        w*p[i] ) >= 97;

subto restRolloX: ( sum <i,w,x,y,z> in IWXYZ :
                        x*p[i] ) >= 610; 

subto restRolloY: ( sum <i,w,x,y,z> in IWXYZ :
                        y*p[i] ) >= 395;

subto restRolloZ: ( sum <i,w,x,y,z> in IWXYZ :
                        z*p[i] ) >= 211;
                        
# *********************************************************************************************************************************